package com.task3;

public class BankAccount {

    private String id;
    private Long accountNumber;
    private String accountName;
    private String bankId;
    private String currency;
    private Double balance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount [" +
                "id='" + id + '\'' +
                ", accountNumber=" + accountNumber +
                ", accountName='" + accountName + '\'' +
                ", bankId='" + bankId + '\'' +
                ", currency='" + currency + '\'' +
                ", balance=" + balance +
                ']';
    }
}
