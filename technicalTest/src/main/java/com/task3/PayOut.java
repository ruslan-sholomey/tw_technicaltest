package com.task3;

public class PayOut {
    private String bankName;
    private Long sourceAccountNumber;
    private String targetBankName;
    private String targetAccountNumber;
    private Double amount;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Long getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public void setSourceAccountNumber(Long sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public String getTargetBankName() {
        return targetBankName;
    }

    public void setTargetBankName(String targetBankName) {
        this.targetBankName = targetBankName;
    }

    public String getTargetAccountNumber() {
        return targetAccountNumber;
    }

    public void setTargetAccountNumber(String targetAccountNumber) {
        this.targetAccountNumber = targetAccountNumber;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "PayOut [" +
                "bankName='" + bankName + '\'' +
                ", sourceAccountNumber=" + sourceAccountNumber +
                ", targetBankName='" + targetBankName + '\'' +
                ", targetAccountNumber=" + targetAccountNumber +
                ", amount=" + amount +
                ']';
    }
}
