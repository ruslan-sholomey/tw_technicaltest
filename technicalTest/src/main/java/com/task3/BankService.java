package com.task3;

import java.util.List;

public class BankService {

    public Bank findBankById(String id) throws Exception{
        List<Bank> banks = new HttpServiceImplTask3().getBankList();
        Bank result = null;
        for (Bank bank:banks) {
            if (bank.getId().equals(id)){
                result = bank;
            }
        }
        return result;
    }
}
