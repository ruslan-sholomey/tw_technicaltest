package com.task3;

import com.HttpServiceUtil;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.util.List;

public class HttpServiceImplTask3 {

    public static void showTaskInformation3() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<Object> call = service.getInformationAboutTask3();
        HttpServiceUtil.response(call);
    }

    public static void startTask3() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<Object> call = service.startTask3();
        HttpServiceUtil.response(call);
    }

    public List<Bank> getBankList() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<List<Bank>> call = service.getBankList();
        return call.execute().body();
    }

    public List<BankAccount> getBankAccountList() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<List<BankAccount>> call = service.getBankAccountList();
        return call.execute().body();
    }

    public List<Payment> getPaymentList() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<List<Payment>> call = service.getPaymentList();
        return call.execute().body();
    }

    public static void payOut(String name, Long sourceAccountNumber, String targetBankName,
                              String targetAccountNumber, Double amount) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<Object> call = service.payOut(name, sourceAccountNumber, targetBankName, targetAccountNumber, amount);
        HttpServiceUtil.response(call);
    }

    public static void finishTask3() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask3 service = retrofit.create(HttpServiceTask3.class);
        Call<Object> call = service.finishTask3();
        HttpServiceUtil.response(call);
    }
}
