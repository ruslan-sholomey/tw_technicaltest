package com.task3;

import java.util.ArrayList;
import java.util.List;

public class PayOutService {

    private List<PayOut> payOuts = new ArrayList<>();
    private PayOut payOut;
    private String bankName;
    private Long sourceAccountNumber;
    private String targetBankName;
    private String targetAccountNumber;
    private Double amount;

    public List<PayOut> payOutList() throws Exception{

        List<Payment> payments = new HttpServiceImplTask3().getPaymentList();
        for (Payment payment : payments){
            payOut = new PayOut();
            bankName = new BankService().findBankById(payment.getRecipientBankId()).getName();
            payOut.setBankName(bankName);
            sourceAccountNumber = new BankAccountService().getBankAccountById(payment.getRecipientBankId()).getAccountNumber();
            payOut.setSourceAccountNumber(sourceAccountNumber);
            targetBankName = new BankService().findBankById(payment.getRecipientBankId()).getName();
            payOut.setTargetBankName(targetBankName);
            targetAccountNumber = payment.getIban();
            payOut.setTargetAccountNumber(targetAccountNumber);
            amount = payment.getAmount();
            payOut.setAmount(amount);

            payOuts.add(payOut);
        }
        return payOuts;
    }

    public static void sendingPayOutRequest() throws Exception{
        List<PayOut> payOuts = new PayOutService().payOutList();
        for (PayOut payOut : payOuts) {
            HttpServiceImplTask3.payOut(payOut.getBankName(), payOut.getSourceAccountNumber(),
                    payOut.getTargetBankName(), payOut.getTargetAccountNumber(), payOut.getAmount());
        }
    }
}
