package com.task3;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

public interface HttpServiceTask3 {
    @GET("task/3?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getInformationAboutTask3();

    @POST("task/3/start?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> startTask3();

    @GET("bank?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<List<Bank>> getBankList();

    @GET("bankAccount?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<List<BankAccount>> getBankAccountList();

    @POST("bank/{bankName}/transfer/{sourceAccountNumber}/{targetBankName}" +
            "/{targetAccountNumber}/{amount}?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> payOut(@Path("bankName") String name, @Path("sourceAccountNumber") Long sourceAccountNumber,
                        @Path("targetBankName") String targetBankName,
                        @Path("targetAccountNumber") String targetAccountNumber,
                        @Path("amount") Double amount);

    @GET("payment?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<List<Payment>> getPaymentList();

    @POST("task/finish?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> finishTask3();
}
