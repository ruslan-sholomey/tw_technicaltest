package com.task3;

import java.util.ArrayList;
import java.util.List;

public class BankAccountService {
    public List<BankAccount> getSourceAccountList() throws Exception{
        List<BankAccount> bankAccountList = new HttpServiceImplTask3().getBankAccountList();
        List<BankAccount> sourceBankAccountList = new ArrayList<>();
        for (BankAccount bankAccount : bankAccountList){
            if (bankAccount.getAccountName().equals("TransferWise Ltd")){
                sourceBankAccountList.add(bankAccount);
            }
        }
        return sourceBankAccountList;
    }

    public List<BankAccount> getTargetAccountList() throws Exception{
        List<BankAccount> bankAccountList = new HttpServiceImplTask3().getBankAccountList();
        List<BankAccount> targetBankAccountList = new ArrayList<>();
        for (BankAccount bankAccount : bankAccountList){
            if (!bankAccount.getAccountName().equals("TransferWise Ltd")){
                targetBankAccountList.add(bankAccount);
            }
        }
        return targetBankAccountList;
    }

    public BankAccount getBankAccountById(String id) throws Exception{
        List<BankAccount> bankAccounts = new BankAccountService().getSourceAccountList();
        BankAccount result = null;
        for (BankAccount bankAccount : bankAccounts){
            if (bankAccount.getBankId().equals(id)){
                result = bankAccount;
            }
        }
        return result;
    }
}
