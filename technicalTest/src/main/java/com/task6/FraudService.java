package com.task6;

import java.util.List;

public class FraudService {

    public static void confirmTransaction() throws Exception{

        List<Payment> payments = new HttpServiceImplTask6().getAllPaymentNotConfirmed();
        for (Payment payment : payments){
            if (payment.getIp().substring(0, 6).equals("37.63.")){
                HttpServiceImplTask6.markTransactionAsFraud(payment.getId());
            }else {
                HttpServiceImplTask6.markTransactionAsNotFraud(payment.getId());
            }
        }
    }
}
