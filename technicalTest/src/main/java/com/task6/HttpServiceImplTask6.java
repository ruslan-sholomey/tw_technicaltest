package com.task6;

import com.HttpServiceUtil;
import com.task5.HttpServiceTask5;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.util.List;

public class HttpServiceImplTask6 {

    public static void getInformationAboutTask6() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<Object> call = service.getInformationAboutTask6();
        HttpServiceUtil.response(call);
    }

    public static void startTask6() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<Object> call = service.startTask6();
        HttpServiceUtil.response(call);
    }

    public List<PaymentHistory> getAllPaymentHistory() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<List<PaymentHistory>> call = service.getAllPaymentHistory();
        return call.execute().body();
    }

    public List<Payment> getAllPaymentNotConfirmed() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<List<Payment>> call = service.getAllPaymentNotConfirmed();
        return call.execute().body();
    }

    public static void markTransactionAsFraud(String id) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<Object> call = service.markTransactionAsFraud(id);
        HttpServiceUtil.response(call);
    }

    public static void markTransactionAsNotFraud(String id) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<Object> call = service.markTransactionAsNotFraud(id);
        HttpServiceUtil.response(call);
    }

    public static void finishTask6() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask6 service = retrofit.create(HttpServiceTask6.class);
        Call<Object> call = service.finishTask6();
        HttpServiceUtil.response(call);
    }
}
