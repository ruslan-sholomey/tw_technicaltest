package com.task6;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface HttpServiceTask6 {
    @GET("task/6?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getInformationAboutTask6();

    @POST("task/6/start?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> startTask6();

    @GET("payment/history?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<List<PaymentHistory>> getAllPaymentHistory();

    @GET("payment?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<List<Payment>> getAllPaymentNotConfirmed();

    @PUT("payment/{id}/fraud?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> markTransactionAsFraud(@Path("id") String id);

    @DELETE("payment/{id}/fraud?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> markTransactionAsNotFraud(@Path("id") String id);

    @POST("task/finish?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> finishTask6();
}
