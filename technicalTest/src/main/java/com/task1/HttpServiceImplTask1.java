package com.task1;

import com.HttpServiceUtil;
import retrofit2.Call;
import retrofit2.Retrofit;

public class HttpServiceImplTask1 {

    public static void showTaskInformation1() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask1 service = retrofit.create(HttpServiceTask1.class);
        Call<Object> call = service.getInformationAboutTask1();
        HttpServiceUtil.response(call);
    }

    public static void startTask1(){
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask1 service = retrofit.create(HttpServiceTask1.class);
        Call<Object> call = service.startTask1();
        HttpServiceUtil.response(call);
    }

    public static void sendingName(String name) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask1 service = retrofit.create(HttpServiceTask1.class);
        Call<Object> call = service.sendName(name);
        HttpServiceUtil.response(call);
    }

    public static void getName() throws Exception {
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask1 service = retrofit.create(HttpServiceTask1.class);
        Call<Object> call = service.getName();
        HttpServiceUtil.response(call);
    }

    public static void finishTask1(){
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask1 service = retrofit.create(HttpServiceTask1.class);
        Call<Object> call = service.finishTask1();
        HttpServiceUtil.response(call);
    }
}
