package com.task1;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HttpServiceTask1 {
    @GET("task/1?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getInformationAboutTask1();

    @POST("task/1/start?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> startTask1();

    @POST("name/{myName}?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> sendName(@Path("myName") String name);

    @GET("name?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getName();

    @POST("task/finish?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> finishTask1();
}
