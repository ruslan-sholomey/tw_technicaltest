package com.task2;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HttpServiceTask2 {
    @GET("task/2?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getInformationAboutTask2();

    @POST("task/2/start?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> startTask2();

    @POST("survivor/{answer}?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> sendingAnswer(@Path("answer") Integer answer);

    @POST("task/finish?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> finishTask2();
}
