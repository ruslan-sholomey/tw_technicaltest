package com.task2;

import java.util.ArrayList;
import java.util.List;

public class GameOfChairsSolution {

    List<Integer> chairsList = new ArrayList<>();

    public Integer gameResult(){

        for (int i = 1; i <= 100; i++){
            chairsList.add(i);
        }

        int currentIndex = 0;
        int skip = 0;

        while (chairsList.size() > 1){
            chairsList.remove(currentIndex);
            skip += 1;
            currentIndex += skip;

            currentIndex %= chairsList.size();
        }
        return chairsList.get(0);
    }
}
