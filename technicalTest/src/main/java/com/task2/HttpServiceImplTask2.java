package com.task2;

import com.HttpServiceUtil;
import retrofit2.Call;
import retrofit2.Retrofit;

public class HttpServiceImplTask2 {

    public static void showTaskInformation2() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask2 service = retrofit.create(HttpServiceTask2.class);
        Call<Object> call = service.getInformationAboutTask2();
        HttpServiceUtil.response(call);
    }

    public static void startTask2() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask2 service = retrofit.create(HttpServiceTask2.class);
        Call<Object> call = service.startTask2();
        HttpServiceUtil.response(call);
    }

    public static void sendingAnswer(Integer answer) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask2 service = retrofit.create(HttpServiceTask2.class);
        Call<Object> call = service.sendingAnswer(answer);
        HttpServiceUtil.response(call);
    }

    public static void finishTask2() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask2 service = retrofit.create(HttpServiceTask2.class);
        Call<Object> call = service.finishTask2();
        HttpServiceUtil.response(call);
    }
}
