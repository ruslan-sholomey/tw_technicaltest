package com.task4;

import com.HttpServiceUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.task3.HttpServiceTask3;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.util.List;

public class HttpServiceImplTask4 {

    public static void showTaskInformation4() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<Object> call = service.getInformationAboutTask4();
        HttpServiceUtil.response(call);
    }

    public static void startTask4() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<Object> call = service.startTask4();
        HttpServiceUtil.response(call);
    }

    public String [] getAllCurrency() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<String []> call = service.getAllCurrency();
        return call.execute().body();
    }

    public Object getAllCompany() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<Object> call = service.getAllCompany();
        return call.execute().body();
    }

    public JsonObject getAllQuote(Integer amount, String sourceCurrency, String targetCurrency) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<JsonObject> call = service.getAllQuote(amount, sourceCurrency, targetCurrency);
        return call.execute().body();
    }

    public Rate getMidRate(String sourceCurrency, String targetCurrency) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<Rate> call = service.getMidRate(sourceCurrency, targetCurrency);
        return call.execute().body();
    }

    public static void sendingHiddenFeePercentage(String companyName, Integer sourceAmount,
                                                  String sourceCurrency, String targetCurrency,
                                                  Double hiddenFeePercentage) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<JsonObject> call = service.sendingHiddenFee(companyName, sourceAmount, sourceCurrency,
                targetCurrency, hiddenFeePercentage);
        HttpServiceUtil.response(call);
    }

    public static void finishTask4() throws Exception {
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask4 service = retrofit.create(HttpServiceTask4.class);
        Call<Object> call = service.finishTask4();
        HttpServiceUtil.response(call);
    }
}
