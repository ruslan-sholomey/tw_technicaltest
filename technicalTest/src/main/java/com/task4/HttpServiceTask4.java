package com.task4;

import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HttpServiceTask4 {
    @GET("task/4?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getInformationAboutTask4();

    @POST("task/4/start?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> startTask4();

    @GET("currency?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<String []> getAllCurrency();

    @GET("company?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> getAllCompany();

    @GET("quote/{amount}/{sourceCurrency}/{targetCurrency}?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<JsonObject> getAllQuote(@Path("amount") Integer amount,
                                 @Path("sourceCurrency") String sourceCurrency,
                                 @Path("targetCurrency") String targetCurrency);

    @GET("rate/midMarket/{sourceCurrency}/{targetCurrency}?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Rate> getMidRate(@Path("sourceCurrency") String sourceCurrency,
                          @Path("targetCurrency") String targetCurrency);

    @POST("hiddenFee/forCompany/{companyName}/{sourceAmount}/{sourceCurrency}" +
            "/{targetCurrency}/{hiddenFeePercentage}?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<JsonObject> sendingHiddenFee(@Path("companyName") String companyName,
                               @Path("sourceAmount") Integer sourceAmount,
                               @Path("sourceCurrency") String sourceCurrency,
                               @Path("targetCurrency") String targetCurrency,
                               @Path("hiddenFeePercentage") Double hiddenFeePercentage);

    @POST("task/finish?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> finishTask4();
}
