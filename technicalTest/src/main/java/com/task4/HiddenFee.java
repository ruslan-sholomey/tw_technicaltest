package com.task4;

public class HiddenFee {
    private String companyName;
    private Integer sourceAmount;
    private String sourceCurrency;
    private String targetCurrency;
    private Double hiddenFeePercentage;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Integer getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(Integer sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public Double getHiddenFeePercentage() {
        return hiddenFeePercentage;
    }

    public void setHiddenFeePercentage(Double hiddenFeePercentage) {
        this.hiddenFeePercentage = hiddenFeePercentage;
    }

    @Override
    public String toString() {
        return "HiddenFee [" +
                "companyName='" + companyName + '\'' +
                ", sourceAmount=" + sourceAmount +
                ", sourceCurrency='" + sourceCurrency + '\'' +
                ", targetCurrency='" + targetCurrency + '\'' +
                ", hiddenFeePercentage=" + hiddenFeePercentage +
                ']';
    }
}
