package com.task4.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.task4.HttpServiceImplTask4;
import com.task4.Quote;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuoteService {

    public List<Quote> getQuoteByCurrency(Integer amount) throws Exception{

        List<String> companyList = new CurrencyAndCompanyService().getCompanyList();
        Map<List<String>, List<String>> mapCurrency = new CurrencyAndCompanyService().getAllCurrency();
        List<Quote> quotes = new ArrayList<>();

        for( Map.Entry<List<String>, List<String>> entry : mapCurrency.entrySet() ){
            for (int i = 0; i < entry.getKey().size(); i++) {
                for (String company : companyList){
                    JsonObject object = new HttpServiceImplTask4().getAllQuote(amount, entry.getKey().get(i), entry.getValue().get(i));
                    Gson gson = new Gson();
                    Quote quote = gson.fromJson(object.get(company).toString(), Quote.class);
                    quote.setNameBank(company);
                    quotes.add(quote);
                }
            }
        }
        return quotes;
    }
}
