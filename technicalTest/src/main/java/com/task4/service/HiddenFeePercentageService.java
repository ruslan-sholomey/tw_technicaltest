package com.task4.service;

import com.task4.HiddenFee;
import com.task4.HttpServiceImplTask4;
import com.task4.Quote;
import com.task4.Rate;

import java.util.ArrayList;
import java.util.List;

public class HiddenFeePercentageService {

    private HiddenFee hiddenFee;

    public List<HiddenFee> getHiddenFeeByAmountCurrency(Integer amount) throws Exception{

        List<Quote> quoteList = new QuoteService().getQuoteByCurrency(amount);
        List<HiddenFee> hiddenFeeList = new ArrayList<>();
        for (Quote quote : quoteList){
            HiddenFee hiddenFee = new HiddenFee();
            hiddenFee.setCompanyName(quote.getNameBank());
            hiddenFee.setSourceAmount(quote.getSourceAmount());
            hiddenFee.setSourceCurrency(quote.getSourceCurrency());
            hiddenFee.setTargetCurrency(quote.getTargetCurrency());
            hiddenFee.setHiddenFeePercentage(new HiddenFeePercentageService().getHiddenFee(quote));
            hiddenFeeList.add(hiddenFee);
        }
        return hiddenFeeList;
    }

    public Double getHiddenFee(Quote quote) throws Exception{
        Rate rate = new HttpServiceImplTask4().getMidRate(quote.getSourceCurrency(), quote.getTargetCurrency());
        Double midRate = rate.getRate();
        Double recipientReceives = quote.getRecipientReceives();
        Double receivesByMidRate = null;
        double hiddenFeePercentage = 0.0;
        if (quote.getCommissionPercentage() == 0.0){
            receivesByMidRate = quote.getYouPay() * midRate;
            hiddenFeePercentage = ((receivesByMidRate - recipientReceives) / receivesByMidRate) * 100;
        }else {
            receivesByMidRate = (quote.getYouPay() - (quote.getYouPay() * quote.getCommissionPercentage())) * midRate;
            hiddenFeePercentage = ((receivesByMidRate - recipientReceives) / receivesByMidRate) * 100;
        }

        int percentage = (int) hiddenFeePercentage;
        double resultPercentage =(double) percentage;
        return resultPercentage;
    }

    public static void sendingHiddenFeePercentage() throws Exception{
        List<HiddenFee> hiddenFeeList = new HiddenFeePercentageService().getHiddenFeeByAmountCurrency(100);
        for (HiddenFee hiddenFee : hiddenFeeList){
            HttpServiceImplTask4.sendingHiddenFeePercentage(hiddenFee.getCompanyName(), hiddenFee.getSourceAmount(),
                    hiddenFee.getSourceCurrency(), hiddenFee.getTargetCurrency(), hiddenFee.getHiddenFeePercentage());
        }
        List<HiddenFee> hiddenFeeList2 = new HiddenFeePercentageService().getHiddenFeeByAmountCurrency(1000);
        for (HiddenFee hiddenFee2 : hiddenFeeList2){
            HttpServiceImplTask4.sendingHiddenFeePercentage(hiddenFee2.getCompanyName(), hiddenFee2.getSourceAmount(),
                    hiddenFee2.getSourceCurrency(), hiddenFee2.getTargetCurrency(), hiddenFee2.getHiddenFeePercentage());
        }
        List<HiddenFee> hiddenFeeList3 = new HiddenFeePercentageService().getHiddenFeeByAmountCurrency(10000);
        for (HiddenFee hiddenFee3 : hiddenFeeList3){
            HttpServiceImplTask4.sendingHiddenFeePercentage(hiddenFee3.getCompanyName(), hiddenFee3.getSourceAmount(),
                    hiddenFee3.getSourceCurrency(), hiddenFee3.getTargetCurrency(), hiddenFee3.getHiddenFeePercentage());
        }
    }
}
