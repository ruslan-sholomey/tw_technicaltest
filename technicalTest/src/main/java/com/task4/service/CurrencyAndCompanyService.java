package com.task4.service;

import com.task4.HttpServiceImplTask4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CurrencyAndCompanyService{

    public List<String> getCurrencyList() throws Exception{
        List<String> currenciesList = new ArrayList<>();
        String [] currencies = new HttpServiceImplTask4().getAllCurrency();
        for (String currency : currencies) {
            currenciesList.add(currency);
        }
        return currenciesList;
    }

    public List<String> getCompanyList() throws Exception{
        List<String> companyList = new ArrayList<>();
        String stringCompany = new HttpServiceImplTask4().getAllCompany().toString();
        String company = stringCompany.substring(12, 123);
        String[] companies = company.split(", ");
        for (String comp : companies) {
            companyList.add(comp);
        }
        return companyList;
    }

    public Map<List<String>, List<String>> getAllCurrency() throws Exception{
        Map<List<String>, List<String>> currencyMap = new HashMap<>();
        List<String> currList = new CurrencyAndCompanyService().getCurrencyList();
        List<String> key = new ArrayList<>();
        List<String> value = new ArrayList<>();
        for (int j = 0; j < currList.size(); j++){
            for (int i = 0; i < currList.size(); i++){
                if (!currList.get(i).equals(currList.get(j))){
                    key.add(currList.get(i).toString());
                    value.add(currList.get(j).toString());
                }
            }
        }
        currencyMap.put(key, value);
        return currencyMap;
    }
}
