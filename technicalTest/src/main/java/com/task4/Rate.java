package com.task4;

public class Rate {
    private Double rate;

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Rate [" +
                "rate=" + rate +
                ']';
    }
}
