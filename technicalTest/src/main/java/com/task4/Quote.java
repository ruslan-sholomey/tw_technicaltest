package com.task4;

public class Quote {
    private String nameBank;
    private double commissionPercentage;
    private String targetCurrency;
    private Double feeInGbp;
    private Double youPay;
    private Double targetValue;
    private String sourceCurrency;
    private Integer sourceAmount;
    private Double offerRate;
    private Double recipientReceives;

    public String getNameBank() {
        return nameBank;
    }

    public void setNameBank(String nameBank) {
        this.nameBank = nameBank;
    }

    public double getCommissionPercentage() {
        return commissionPercentage;
    }

    public void setCommissionPercentage(double commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    public String getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    public Double getFeeInGbp() {
        return feeInGbp;
    }

    public void setFeeInGbp(Double feeInGbp) {
        this.feeInGbp = feeInGbp;
    }

    public Double getYouPay() {
        return youPay;
    }

    public void setYouPay(Double youPay) {
        this.youPay = youPay;
    }

    public Double getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(Double targetValue) {
        this.targetValue = targetValue;
    }

    public String getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(String sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public Integer getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(Integer sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public Double getOfferRate() {
        return offerRate;
    }

    public void setOfferRate(Double offerRate) {
        this.offerRate = offerRate;
    }

    public Double getRecipientReceives() {
        return recipientReceives;
    }

    public void setRecipientReceives(Double recipientReceives) {
        this.recipientReceives = recipientReceives;
    }

    @Override
    public String toString() {
        return "Quote{" +
                "nameBank='" + nameBank + '\'' +
                ", commissionPercentage=" + commissionPercentage +
                ", targetCurrency='" + targetCurrency + '\'' +
                ", feeInGbp=" + feeInGbp +
                ", youPay=" + youPay +
                ", targetValue=" + targetValue +
                ", sourceCurrency='" + sourceCurrency + '\'' +
                ", sourceAmount=" + sourceAmount +
                ", offerRate=" + offerRate +
                ", recipientReceives=" + recipientReceives +
                '}';
    }
}
