package com.task5;

import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface HttpServiceTask5 {
    @GET("task/5?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<JsonObject> getInformationAboutTask5();

    @POST("task/5/start?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> startTask5();

    @GET("payment?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<List<Payment>> getAllPayment();

    @PUT("payment/{id}/aml?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> markTransactionAsPep(@Path("id") String id);

    @DELETE("payment/{id}/aml?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> markTransactionAsNotPep(@Path("id") String id);

    @POST("task/finish?token=c191e11b1cc4ad92fa310130c0f4fbeb")
    Call<Object> finishTask5();

}
