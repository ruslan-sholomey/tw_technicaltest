package com.task5;

import com.HttpServiceUtil;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.Retrofit;

import java.util.List;

public class HttpServiceImplTask5 {

    public JsonObject showTaskInformation5() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask5 service = retrofit.create(HttpServiceTask5.class);
        Call<JsonObject> call = service.getInformationAboutTask5();
        return call.execute().body();
    }

    public static void startTask5() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask5 service = retrofit.create(HttpServiceTask5.class);
        Call<Object> call = service.startTask5();
        HttpServiceUtil.response(call);
    }

    public List<Payment> getAllPayment() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask5 service = retrofit.create(HttpServiceTask5.class);
        Call<List<Payment>> call = service.getAllPayment();
        return call.execute().body();
    }

    public static void markTransactionAsPep(String id) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask5 service = retrofit.create(HttpServiceTask5.class);
        Call<Object> call = service.markTransactionAsPep(id);
        HttpServiceUtil.response(call);
    }

    public static void markTransactionAsNotPep(String id) throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask5 service = retrofit.create(HttpServiceTask5.class);
        Call<Object> call = service.markTransactionAsNotPep(id);
        HttpServiceUtil.response(call);
    }

    public static void finishTask5() throws Exception{
        Retrofit retrofit = HttpServiceUtil.getRetrofit();
        HttpServiceTask5 service = retrofit.create(HttpServiceTask5.class);
        Call<Object> call = service.finishTask5();
        HttpServiceUtil.response(call);
    }
}
