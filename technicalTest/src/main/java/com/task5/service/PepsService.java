package com.task5.service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.task5.HttpServiceImplTask5;
import com.task5.Payment;
import com.task5.Peps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PepsService {

    public List<Peps> getAllPeps() throws Exception{

        List<Peps> pepsList = new ArrayList<>();
        JsonObject object = new HttpServiceImplTask5().showTaskInformation5();
        Gson gson = new Gson();
        JsonArray jsonArray = gson.fromJson(object.get("peps"), JsonArray.class);
        for (JsonElement jsonElement : jsonArray){
            String [] strings = jsonElement.getAsString().split(" - ");
            List<String> list = Arrays.asList(strings);
            Peps peps = new Peps();
            peps.setName(list.get(0));
            peps.setCountry(list.get(1));
            pepsList.add(peps);
        }
        return pepsList;
    }

    public static void sendingPepRequest() throws Exception{

        List<Peps> pepsList = new PepsService().getAllPeps();
        List<Payment> paymentList = new HttpServiceImplTask5().getAllPayment();
        for (Payment payment : paymentList){
            for (Peps peps : pepsList){
                if (peps.getName().equals(payment.getRecipientName())){
                    HttpServiceImplTask5.markTransactionAsPep(payment.getId());
                }
            }
        }
        for (Payment payment : paymentList){
            for (Peps peps : pepsList){
                if (!peps.getName().equals(payment.getRecipientName())){
                    HttpServiceImplTask5.markTransactionAsNotPep(payment.getId());
                }
            }
        }
    }
}
